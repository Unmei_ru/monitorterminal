import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQml.Models 2.2
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1
import Qt.labs.settings 1.0
import QtGraphicalEffects 1.0
import QtQuick.Extras 1.4

import ConnectionManager 1.0

ApplicationWindow {
    id: mainWindow
    property var listButtons : []
    property var listIndicators : []

    property int _micVolume : 0
    property int _speakerVolume : 0

    property int _micVolumeAudio : 100
    property int _speakerVolumeAudio : 100
    property var _ip: "0.0.0.0"

    Settings {
        property alias micVolume: mainWindow._micVolumeAudio
        property alias speakerVolume: mainWindow._speakerVolumeAudio
    }

    function resetAllData()
    {

        console.log("QML: Received restart command.");
        abonentsModel.clear();

        var element;

        for (var i=0; i<listButtons.length; i++)
        {
            element = listButtons[i];

            if (element._mode > 0){ //При рестарте для доп кнопок
                element.setEnabed( true ) //Режим включить
                element.setColor ( 3 ) //Цвет в зеленый
            }else{                  //При рестарте для основных кнопок полный сброс
                element.setEnabed( 0 ) //Режим
                element.setColor ( 0 ) //Цвет
            }

            element.setBlink ( 0 ) //Мигание
        }

        for (var j=0; j<listIndicators.length; j++)
        {
            element = listIndicators[j];
            element.setEnabed( 0 ) //Режим
            element.setColor ( 0 ) //Цвет
            element.setBlink ( 0 ) //Мигание
        }

        var pin = 0;
        var directive = 1;
        var data = null;

        console.log("QML: Complite restart.")

        ConnectionManager.sendMessage(pin, directive,  data);

    }

    Component.onCompleted: {
        stackView.push("qrc:/TabBarPage.qml")

        resetAllData();

        ConnectionManager.setAudioVolume(_speakerVolumeAudio);
        ConnectionManager.setAudioMicVolume(_micVolumeAudio);
    }

    Connections{
        target: ConnectionManager

        onSignalMessageRecived:{

            console.log("QML: msg recived " + pin + ", " + directive);

            switch ( directive ){

            case 1: // Перегрузить ПО пульта
            //ConnectionManager.reboot();
                resetAllData();
            break;
            case 2: // Установить отображение уровней громкости и чувствительности микрофона;
                var volume   = new Uint8Array(msgData);
                _speakerVolume = volume[0];
                _micVolume = volume[1];
            break;
            case 3: // Заблокировать работу пульта
                blockDialog.open()
            break;
            case 4: // Разблокировать работу пульта
                blockDialog.close()
            break;
            case 5: // Изменить состояние отображения элемента
//                (кнопки, индикатора и т.д.)
//                с номером PIN

                var mode   = new Uint8Array(msgData);

                var element;

                for (var i=0; i<listButtons.length; i++) {
                    element = listButtons[i];
                    if (element.getPin() == pin){

                        element.setEnabed( mode[0] ) //Режим
                        element.setColor ( mode[1] ) //Цвет
                        element.setBlink ( mode[2] ) //Мигание

                        return;
                    }
                }

                for (var i=0; i<listIndicators.length; i++) {
                    element = listIndicators[i];
                    if (element.getPin() == pin){

                        element.setEnabed( mode[0] ) //Режим
                        element.setColor ( mode[1] ) //Цвет
                        element.setBlink ( mode[2] ) //Мигание

                        return;
                    }
                }

            break;
            case 6: //Внести в список соединений
//                Строковые имена абонентов
//                Соединенных между собой

                var usersPair   = new Uint16Array(msgData);

                console.log( "Inser pair A = " + usersPair[0] + " B = " + usersPair[1]);

                var nameA = "Безымянный";
                var nameB = "Безымянный";

                var pinA = usersPair[0];
                var pinB = usersPair[1];

                var isFindA = false;
                var isFindB = false;

                for (var i=0; i<listButtons.length; i++) {
                    element = listButtons[i];

                    if (element.getPin() == pinA){
                            nameA = element.getTitle();
                            isFindA = true;
                    }

                    if (element.getPin() == pinB){
                            nameB = element.getTitle();
                            isFindB = true;
                    }

                    if (isFindA && isFindB) break;
                }
                var pairText =    nameA + " - " + nameB;

                for (var i=0; i<abonentsModel.count; i++) {

                    var valueA = abonentsModel.get(i).valueA;
                    var valueB = abonentsModel.get(i).valueB;

                    if( (pinA == valueA) && (pinB == valueB) ){
                        return;
                    }
                }

                abonentsModel.append({ "text": pairText, "valueA" : pinA, "valueB": pinB  })

            break;
            case 7: //Удалить из  списка соединений
//                Строковые имена абонентов
//                Соединенных между собой

                var usersPair   = new Uint16Array(msgData);
                console.log( "Delete pair A = " + usersPair[0] + " B = " + usersPair[1]);
                var pinA = usersPair[0];
                var pinB = usersPair[1];

                for (var i=0; i<abonentsModel.count; i++) {

                    var valueA = abonentsModel.get(i).valueA;
                    var valueB = abonentsModel.get(i).valueB;

                    if( (pinA == valueA) && (pinB == valueB) ){

                        abonentsModel.remove(i);
                        break;
                    }

                }

            break;
            default:

            }

        }

        onSignalAddonEvent:{

            var element;

            for (var i=0; i<listButtons.length; i++) {
                element = listButtons[i];


                if (mode == 1){


                if ((element.getMode() == 1) || (element.getMode() == 4)){ //Расширенный протокол: кнопка А



                    switch (eventId){
                    case 1: //Пришел "вызов"

                        console.log("Recive call from host " + ip);

                        if ((element.getIP() == ip)  || ("::ffff:" + element.getIP() ==  ip)){
                            element.setColor ( 4 ) //При совпадении IP ставим красный
                            element.setToggled( true ) //Кнопка нажатая
                            element.setBlink ( 1 ) //Моргаем
                            element.setCall(true) //Устанавливем флаг вызова
                            element.setTalk(false) //Устанавливаем разговор
                            element.setEnabed( true ) //Режим
                            //return;
                        }
                        break;

                    case 2: //Пришел "ответ"



                        if ((element.getIP() == ip)  || ("::ffff:" + element.getIP() == ip)){ //Для кнопки с привязаным IP

                            console.log("Recive replay from host " + ip + " c " + element.getCall());

                            element.setColor ( 4 ) //При совпадении IP ставим красный
                            element.setBlink ( 0 ) //Не моргаем
                            element.setCall( false ) //Убираем звонок/вызов
                            element.setTalk( true ) //Устанавливаем разговор
                            element.setEnabed( true ) //Режим
                        }else{ //Остальные отбиваем

                            console.log("Остальные отбиваем " + element.getIP() + " c " + element.getCall());
                            if (!element.getCall()){   //Если не вызов
                                element.setColor ( 3 ) //Сбрасываем в зеленый
                                element.setBlink ( 0 ) //Не моргаем

                                if (element.getTalk()) element.endCall()

                                element.setTalk( false )

                            }
                        }
                        break;
                    case 3: //Пришел "Отбой"
                        if ((element.getIP() == ip) || ("::ffff:" + element.getIP() == ip)){
                            element.setColor ( 3 ) //При совпадении IP ставим зеленый
                            element.setBlink ( 0 ) //Не моргаем
                            element.setTalk( false )
                            element.setCall( false )
                            //return;
                        }
                        break;
                    case 4: //Послать всем активным звонкам отбой
                        if (element.getTalk()){
                            element.setColor ( 3 ) //При совпадении IP ставим зеленый
                            element.setBlink ( 0 ) //Не моргаем
                            element.setTalk( false )
                            element.setCall( false )
                        }
                        break;
                    default:
                        break;

                    }
                }
                }else if (mode == 2){


                    if (element.getMode() == 2){ //Расширенный протокол: кнопка Б

                        if ((element.getIP() == ip) || ("::ffff:" + element.getIP() == ip)){

                            if (element.getCode() == code){
                                if (eventId == 0){
                                    element.setColor ( 3 ) //При совпадении IP ставим зеленый
                                }else if (eventId == 1){
                                    element.setColor ( 4 ) //При совпадении IP ставим зеленый
                                }
                                return;
                            }
                        }

                    }
                }
            }

        }


        onSignalIPInfo:{

            _ip = ip

        }

        onSignalPingEnable:{

            var element;

            for (var i=0; i<listButtons.length; i++) {
                element = listButtons[i];

                if ((element.getMode() == 1) || (element.getMode() == 4)){

                        element.setEnabed( true )
                }
            }
        }

        onSignalPingFail:{

            var element;

            for (var i=0; i<listButtons.length; i++) {
                element = listButtons[i];


                if ((element.getMode() == 1) || (element.getMode() == 4)){

                        //element.setEnabed( true )

                        if ((element.getIP() == ip) || ("::ffff:" + element.getIP() == ip)) element.setEnabed( false );
                }
            }
        }
    }

    ListModel {
        id: abonentsModel
    }



    Component {
        id: indicatorDelegate

        StatusIndicatorMaterial {
            _pin : pin
            _text: title

            Layout.fillWidth: true
            Layout.fillHeight: true

            Layout.column: pos_x
            Layout.row: pos_y

            Component.onCompleted: {
            }
        }
    }

    Component {
        id: controlDelegate

        ButtonMaterial {
            _pin : pin
            _text: title
            _color: color

            _mode:  mode
            _ip: ip

            _code: code

            Layout.fillWidth: true
            Layout.fillHeight: true

            Layout.column: pos_x
            Layout.row: pos_y

        }
    }

    visible: true
    title: Qt.application.name

    header: ToolBar {
        Material.foreground: "white"
        Material.elevation: 0


        height: 100 //* dp

        Material.background: c_DARK_PRIMARY_COLOR

        RowLayout {
            spacing: 20
            anchors.fill: parent

            Label {
                id: titleLabel
                text: text_TITLE
                font.pixelSize: 35
                font.bold: true
                elide: Label.ElideRight
                Layout.leftMargin: 40
                horizontalAlignment: parent.horizontalCenter
            }

            Item {
                Layout.fillWidth: true
            }

            ColumnLayout{

                Label {
                    id: timeLabel
                    property var _time: new Date().toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");

                    text: _time
                    font.pixelSize: 20
                    font.bold: true
                    elide: Label.ElideRight
                    Layout.rightMargin: 20

                    function timeChanged() {
                        var date = new Date;
                        _time = date.toTimeString().replace(/.*(\d{2}:\d{2}:\d{2}).*/, "$1");
                    }

                    Timer
                    {
                        interval: 1000; running: true; repeat: true
                        onTriggered: timeLabel.timeChanged();
                    }
                }

                ToolButton {
                    anchors.right: parent.right
                    contentItem: Image {
                        fillMode: Image.Pad
                        horizontalAlignment: Image.AlignHCenter
                        verticalAlignment: Image.AlignVCenter
                        source: "images/menu.png"
                    }
                    onClicked: optionsMenu.open()

                    Menu {
                        id: optionsMenu
                        x: parent.width - width
                        transformOrigin: Menu.TopRight

//                        MenuItem {
//                            text: "Настройки пульта"
//                            onTriggered: settingsDialog.open()

//                        }
                        MenuItem {
                            text: "Настройки"
                            onTriggered: settingsDialogAudio.open()

                        }

                        MenuItem {
                            text: "О программе"
                            onTriggered: aboutDialog.open()
                        }
                    }
                }

            }

        }
    }

    Pane {
        id: statusPane
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 0

        Material.elevation: 2
        height: 30

        Material.background: c_PRIMARY_COLOR

        JSONListModel {
            id: indicatorModel
            source: "file:" +main_DIR + "jsonData.json"
            query: "$.terminal.indicator[*]"

        }

        GridLayout{
            id: girdStatus
            anchors.fill: parent
            anchors.margins: -10

            columns: 6
            rows: 1

            Repeater{
                model: 6
                Item{
                    Layout.fillWidth: true
                    Layout.fillHeight: true
                }
            }

            Repeater{
                model: indicatorModel.model
                delegate: indicatorDelegate
            }
        }
    }

    Pane {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 10
        anchors.topMargin: 60
        Material.elevation: 1

        width: parent.width * 0.70


        StackView {
            id: stackView
            anchors.fill: parent
            anchors.margins: 0

            initialItem: Pane {
                id: pane

                BusyIndicator {
                    anchors.horizontalCenter: parent.horizontalCenter
                    anchors.verticalCenter: parent.verticalCenter
                }
            }
        }
    }

    Component {
        id: itemDelegateComponent

        ItemDelegate {
            text: labelText
            width: parent.width
        }
    }

    Pane {
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.right: parent.right
        anchors.margins: 10
        anchors.topMargin: 60
        Material.elevation: 1

        width: parent.width * 0.25

        ListView {
            id: listView
            anchors.fill: parent
            clip: true

            model: abonentsModel

            section.delegate: Pane {
                width: listView.width
                height: sectionLabel.implicitHeight + 20

                Label {
                    id: sectionLabel
                    text: section
                    anchors.centerIn: parent
                }
            }

            delegate: Loader {
                id: delegateLoader
                width: listView.width
                sourceComponent: itemDelegateComponent

                property string labelText: text
                property ListView view: listView
                property int ourIndex: index

                ListView.onRemove: SequentialAnimation {
                    PropertyAction {
                        target: delegateLoader
                        property: "ListView.delayRemove"
                        value: true
                    }
                    NumberAnimation {
                        target: item
                        property: "height"
                        to: 0
                        easing.type: Easing.InOutQuad
                    }
                    PropertyAction {
                        target: delegateLoader
                        property: "ListView.delayRemove"
                        value: false
                    }
                }
            }
        }
    }

    Dialog {
        id: blockDialog
        modal: true
        focus: true
        title: "Пульт заблокирован"
        x: (window.width - width) / 2
        y: window.height / 6
        width: Math.min(window.width, window.height) / 3 * 2
        contentHeight: blockColumn.height

        closePolicy: Dialog.NoAutoClose

        Column {
            id: blockColumn
            spacing: 20

            Label {
                width: blockDialog.availableWidth
                text: "Терминал был удаленно заблокирован."
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }
        }

    }

    Dialog {
        id: aboutDialog
        modal: true
        focus: true
        title: "О программе"
        x: (window.width - width) / 2
        y: window.height / 6
        width: Math.min(window.width, window.height) / 3 * 2
        contentHeight: aboutColumn.height

        Column {
            id: aboutColumn
            spacing: 20

            Label {
                width: aboutDialog.availableWidth
                text: "Пульт оператора системы громкоговорящей связи \"ГГС-ЛИНСИС\"."
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }

            Label {
                width: aboutDialog.availableWidth
                text: "Версия программного обеспечения v1.05. \n"
                      + "Все права защищены @ 2019 ООО \"ЛИНСИС\". \n"
                      + "WWW: www.lin-sys.ru \n"
                      + "E-MAIL: support@lin-sys.ru \n"
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }

            Label {
                width: aboutDialog.availableWidth
                text: "IP:" + _ip
                wrapMode: Label.Wrap
                font.pixelSize: 12
            }
        }
    }

    Dialog {
        id: settingsDialog
        modal: true
        focus: true
        title: "Настройки звука (пульт)"
        x: (window.width - width) / 2
        y: window.height / 6
        width: Math.min(window.width, window.height) / 3 * 2
        contentHeight: settingsLayout.height

        GridLayout{
            id: settingsLayout
            width: parent.width
            rows: 2
            columnSpacing: 20

            Image {
                Layout.row: 0
                Layout.column: 0

                source: "images/speaker_black_96x96.png"

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: c_PRIMARY_COLOR
                }


            }

            RoundButton {
                id: volumePlus
                scale: 0.5
                Layout.row: 0
                Layout.column: 1


                onClicked:{

                    var pin = 0;
                    var directive = 2;

                    var buffer = new ArrayBuffer(2);

                    var volume   = new Uint8Array(buffer);

                    volume[0] = _speakerVolume + 1;
                    volume[1] = _micVolume;

                    var data = buffer;
                    ConnectionManager.sendMessage(pin, directive,  data);

                }

                contentItem: Image {
                    width: volumePlus.width
                    height: volumePlus.height
                    source: "images/add_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR 
                    }
                }
            }

            Label{
                Layout.row: 0
                Layout.column: 2

                text: _speakerVolume

                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter

                font.pixelSize: 50
            }

            RoundButton {
                id: volumeMinus
                scale: 0.5
                Layout.row: 0
                Layout.column: 3

                onClicked:{

                    var pin = 0;
                    var directive = 2;

                    var buffer = new ArrayBuffer(2);

                    var volume   = new Uint8Array(buffer);

                    if (_speakerVolume >= 1){
                        volume[0] = _speakerVolume - 1;
                    }else{
                        volume[0] = 0;
                    }

                    volume[1] = _micVolume;

                    var data = buffer;
                    ConnectionManager.sendMessage(pin, directive,  data);

                }

                contentItem: Image {
                    width: volumeMinus.width
                    height: volumeMinus.height
                    source: "images/remove_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR 
                    }
                }
            }

            Image {
                Layout.row: 1
                Layout.column: 0

                source: "images/mic_black_96x96.png"

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: c_PRIMARY_COLOR  
                }
            }

            RoundButton {
                id: micPlus
                scale: 0.5
                Layout.row: 1
                Layout.column: 1

                onClicked:{

                    var pin = 0;
                    var directive = 2;

                    var buffer = new ArrayBuffer(2);

                    var volume   = new Uint8Array(buffer);

                    volume[0] = _speakerVolume;
                    volume[1] = _micVolume + 1;

                    var data = buffer;
                    ConnectionManager.sendMessage(pin, directive,  data);

                }

                contentItem: Image {
                    width: micPlus.width
                    height: micPlus.height
                    source: "images/add_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR 
                    }
                }
            }

            Label{
                Layout.row: 1
                Layout.column: 2

                text: _micVolume
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter

                font.pixelSize: 50
            }

            RoundButton {
                id: micMinus
                scale: 0.5
                Layout.row: 1
                Layout.column: 3

                onClicked:{

                    var pin = 0;
                    var directive = 2;

                    var buffer = new ArrayBuffer(2);

                    var volume   = new Uint8Array(buffer);

                    volume[0] = _speakerVolume;

                    if (_micVolume >= 1){
                        volume[1] = _micVolume - 1;
                    }else{
                        volume[1] = 0;
                    }

                    var data = buffer;
                    ConnectionManager.sendMessage(pin, directive,  data);

                }

                contentItem: Image {
                    width: micMinus.width
                    height: micMinus.height
                    source: "images/remove_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR 
                    }
                }
            }

        }
    }

    Dialog {
        id: settingsDialogAudio
        modal: true
        focus: true
        title: "Настройки звука (система)"
        x: (window.width - width) / 2
        y: window.height / 6
        width: Math.min(window.width, window.height) / 3 * 2
        contentHeight: settingsLayout.height

        GridLayout{
            id: settingsLayoutAudio
            width: parent.width
            rows: 2
            columnSpacing: 20

            Image {
                Layout.row: 0
                Layout.column: 0

                source: "images/speaker_black_96x96.png"

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: c_PRIMARY_COLOR
                }


            }

            RoundButton {
                id: volumePlusAudio
                scale: 0.5
                Layout.row: 0
                Layout.column: 1


                onClicked:{

                    if (_speakerVolumeAudio < 100) _speakerVolumeAudio+=10;
                    ConnectionManager.setAudioVolume(_speakerVolumeAudio);

                }

                contentItem: Image {
                    width: volumePlusAudio.width
                    height: volumePlusAudio.height
                    source: "images/add_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR
                    }
                }
            }

            Label{
                Layout.row: 0
                Layout.column: 2

                text: _speakerVolumeAudio + "%"

                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter

                font.pixelSize: 30

                visible: true
            }

            RoundButton {
                id: volumeMinusAudio
                scale: 0.5
                Layout.row: 0
                Layout.column: 3

                onClicked:{

                    if (_speakerVolumeAudio > 0) _speakerVolumeAudio-=10;
                    ConnectionManager.setAudioVolume(_speakerVolumeAudio);

                }

                contentItem: Image {
                    width: volumeMinusAudio.width
                    height: volumeMinusAudio.height
                    source: "images/remove_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR
                    }
                }
            }

            Image {
                Layout.row: 1
                Layout.column: 0

                source: "images/mic_black_96x96.png"

                ColorOverlay {
                    anchors.fill: parent
                    source: parent
                    color: c_PRIMARY_COLOR
                }
            }

            RoundButton {
                id: micPlusAudio
                scale: 0.5
                Layout.row: 1
                Layout.column: 1

                onClicked:{

                    if (_micVolumeAudio < 100) _micVolumeAudio+=10;
                    ConnectionManager.setAudioMicVolume(_micVolumeAudio);

                }

                contentItem: Image {
                    width: micPlusAudio.width
                    height: micPlusAudio.height
                    source: "images/add_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR
                    }
                }
            }

            Label{
                Layout.row: 1
                Layout.column: 2

                text: _micVolumeAudio + "%"
                horizontalAlignment: Label.AlignHCenter
                verticalAlignment: Label.AlignVCenter

                font.pixelSize: 30

                visible: true
            }

            RoundButton {
                id: micMinusAudio
                scale: 0.5
                Layout.row: 1
                Layout.column: 3

                onClicked:{

                    if (_micVolumeAudio > 0) _micVolumeAudio-=10;
                    ConnectionManager.setAudioMicVolume(_micVolumeAudio);
                }

                contentItem: Image {
                    width: micMinusAudio.width
                    height: micMinusAudio.height
                    source: "images/remove_circle_black_96x96.png"

                    ColorOverlay {
                        anchors.fill: parent
                        source: parent
                        color: c_PRIMARY_COLOR
                    }
                }
            }

        }
    }


    footer: Pane {

        width: parent.width
        height: 120

        JSONListModel {
            id: controlModel
            source: "file:" +main_DIR + "jsonData.json"
            query: "$.terminal.function[*]"

        }

        GridLayout {
            anchors.fill: parent
            columns: 5
            rows: 2

            Repeater{
                model: 10
                Item{
                    Layout.fillWidth: true
                }
            }

            Repeater{
                model: controlModel.model
                delegate: controlDelegate
            }

        }

    }


}

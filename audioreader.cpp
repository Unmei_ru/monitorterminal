#include "audioreader.h"
#include <QDebug>
#include <QThread>


AudioReader::AudioReader(QObject *parent) : QObject(parent)
{

    buffer_frames = 160;
    m_rate = 16000;
    m_format = SND_PCM_FORMAT_S16_LE;

    int res = openCapture();

    qDebug() << "ALSA = " <<  res;

    /*

      if ((err = snd_pcm_open (&capture_handle, "default", SND_PCM_STREAM_CAPTURE, 0)) < 0) {
        qDebug ("cannot open audio device %s (%s)\n",
                 "default",
                 snd_strerror (err));
        exit (1);
      }

      qDebug("audio interface opened\n");

      if ((err = snd_pcm_hw_params_malloc (&hw_params)) < 0) {
        qDebug ("cannot allocate hardware parameter structure (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "hw_params allocated\n");

      if ((err = snd_pcm_hw_params_any (capture_handle, hw_params)) < 0) {
        qDebug ( "cannot initialize hardware parameter structure (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "hw_params initialized\n");

      if ((err = snd_pcm_hw_params_set_access (capture_handle, hw_params, SND_PCM_ACCESS_RW_INTERLEAVED)) < 0) {
        qDebug ( "cannot set access type (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "hw_params access setted\n");

      if ((err = snd_pcm_hw_params_set_format (capture_handle, hw_params, format)) < 0) {
        qDebug ( "cannot set sample format (%s)\n",
                 snd_strerror (err));
        return;
      }

      fprintf(stdout, "hw_params format setted\n");

      if ((err = snd_pcm_hw_params_set_rate_near (capture_handle, hw_params, &rate, 0)) < 0) {
        qDebug ( "cannot set sample rate (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "hw_params rate setted\n");

      if ((err = snd_pcm_hw_params_set_channels (capture_handle, hw_params, 1)) < 0) {
        qDebug ( "cannot set channel count (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "hw_params channels setted\n");

      if ((err = snd_pcm_hw_params (capture_handle, hw_params)) < 0) {
        qDebug ( "cannot set parameters (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "hw_params setted\n");

      snd_pcm_hw_params_free (hw_params);

      qDebug( "hw_params freed\n");

      if ((err = snd_pcm_prepare (capture_handle)) < 0) {
        qDebug ( "cannot prepare audio interface for use (%s)\n",
                 snd_strerror (err));
        return;
      }

      qDebug( "audio interface prepared\n");
      */
}

void AudioReader::slotReadAudio()
{
    int res =  capturceFunc();

    /*
    buffer.resize(buffer_frames * snd_pcm_format_width(m_format) / 8 );

    qDebug() << "buffer allocated" << buffer.size() << snd_pcm_format_width(m_format);

    int i = 0;

    snd_pcm_readi (capture_handle, buffer.data(), buffer_frames);
    snd_pcm_sframes_t fFrames = 0;

    while(true) {


            snd_pcm_readi (capture_handle, buffer.data(), buffer_frames);

            fFrames = snd_pcm_avail(capture_handle);
            emit signalMicData(buffer);
            //qDebug() << etimer.restart() << fFrames;

            if (fFrames > 960) snd_pcm_readi (capture_handle, buffer.data(), buffer_frames);

            QThread::msleep(10);




        //if ((err = snd_pcm_readi (capture_handle, buffer.data(), buffer_frames)) != buffer_frames) {
        //  qDebug ( "read from audio interface failed (%s)\n",
       //            err, snd_strerror (err));
        //  return;
       // }
        //i++;

       // emit signalMicData(buffer);

        //while (snd_pcm_forwardable(capture_handle) > 3200) {
        //    snd_pcm_readi (capture_handle, buffer.data(), buffer_frames);
        //}
       // QThread::msleep(8);




       }


      snd_pcm_close (capture_handle);
      qDebug( "audio interface closed\n");
      */
}


int AudioReader::openCapture ()
{
    snd_pcm_hw_params_t* params;
    int result;
    snd_pcm_uframes_t tmp_buf_size;
    snd_pcm_uframes_t tmp_period_size;

    result = snd_pcm_open (&capture_handle,
                           "default",
                           SND_PCM_STREAM_CAPTURE,
                           0);
    if (result < 0)
        return -1;

    /* Allocate a hardware parameters object. */
    snd_pcm_hw_params_alloca (&hw_params);

    /* Fill it in with default values. */
    snd_pcm_hw_params_any (capture_handle, hw_params);

    /* Set interleaved mode */
    snd_pcm_hw_params_set_access (capture_handle, hw_params,
                                  SND_PCM_ACCESS_RW_INTERLEAVED);

    /* Set format */


    snd_pcm_hw_params_set_format (capture_handle, hw_params, m_format);

    /* Set number of channels */
    snd_pcm_hw_params_set_channels (capture_handle, hw_params, 1);

    /* Set clock rate */
    m_rate = 16000;
    snd_pcm_hw_params_set_rate_near (capture_handle, hw_params, &m_rate, NULL);

    /* Set period size to samples_per_frame frames. */
    tmp_period_size = buffer_frames;
    snd_pcm_hw_params_set_period_size_near (capture_handle, hw_params,
                                            &tmp_period_size, NULL);
    buffer_frames = tmp_period_size > buffer_frames ? tmp_period_size :
                                                      buffer_frames;

    /* Set the sound device buffer size and latency */
    int INPUT_LATENCY = 100;

    tmp_buf_size = (m_rate / 1000) * INPUT_LATENCY;

    snd_pcm_hw_params_set_buffer_size_near (capture_handle, hw_params,
                                            &tmp_buf_size);
    input_latency_ms = tmp_buf_size / (m_rate / 1000);

    /* Set our buffer */
    ca_buf_size = buffer_frames * 2;

    //stream->ca_buf = (char*) pj_pool_alloc (stream->pool, stream->ca_buf_size);


    /* Activate the parameters */
    result = snd_pcm_hw_params (capture_handle, hw_params);
    if (result < 0) {
        snd_pcm_close (capture_handle);
        return -1;
    }

    return 0;
}

int AudioReader::capturceFunc()
{

    snd_pcm_t* pcm             = capture_handle;
    int size                   = ca_buf_size;
    snd_pcm_uframes_t nframes  = buffer_frames;

    int result;
    struct sched_param param;
    pthread_t* thid;

    buffer.resize( size );

    snd_pcm_prepare (pcm);

    while (true) {


        result = snd_pcm_readi (pcm, buffer.data(), nframes);
        if (result == -EPIPE) {
            snd_pcm_prepare (pcm);
            continue;
        } else if (result < 0) {

        }

        emit signalMicData( buffer );

        //QThread::msleep(input_latency_ms);
        //qDebug() << etimer.restart()  << snd_pcm_avail(pcm);

    }

    snd_pcm_drop(pcm);

    return 0;
}

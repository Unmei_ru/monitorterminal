#ifndef UDPPROTOCOLMANAGER_H
#define UDPPROTOCOLMANAGER_H

#define SERVER_PORT 65001
#define CLIENT_PORT 65001

#define PACKET_HEADER_SIZE 6

#define PACKET_AUDIO 1
#define PACKET_CONTROL 2
#define PACKET_COMMAND 3
#define PACKET_PING 4

#define MSG_CALL 1
#define MSG_REPlY 2
#define MSG_END 3

#include <QObject>
#include <QThread>
#include <QUdpSocket>
#include <QHostAddress>
#include <QMutex>
#include <QElapsedTimer>

#include "zeropacketsender.h"

class UDPProtocolManager : public QObject
{
    Q_OBJECT
public:
    explicit UDPProtocolManager(QObject *parent = nullptr);


    void AddButton(QHostAddress host);

    void AddHost(QHostAddress host);
    void RemoveHost(QHostAddress host);
    bool isHostsEmpty();

    void WriteControl(QByteArray buf, QHostAddress host);
    void WriteCommand(QByteArray buf, QHostAddress host);

    void WritePing();

    QList<quint32> CheckPing();

    QHostAddress localIp() const;

    void ClearBuffer();

signals:
    void signalPlayAudio(QByteArray audioData);
    void signalContolMsg(quint8 msgCode, QHostAddress host);
    void signalWriteDatagram(const QByteArray &datagram, const QHostAddress &host, quint16 port);
public slots:
    void StartUDPServer();

    void slotInit();
    void slotSendAudio(QByteArray audioData);
    void slotWriteDatagram(const QByteArray &datagram, const QHostAddress &host, quint16 port);
private:
    QUdpSocket *m_socket;
    QList<quint32> m_hostsList;

    QList<quint32> m_buttonsList;
    QList<quint32> m_okList;

    QByteArray m_audioBuffer;
    QMutex m_mutex;

    quint16 m_counter;

    QElapsedTimer m_packetTimer;

    QHostAddress m_localIp;

    ZeroPacketSender *m_zeroSender;

    QByteArray GenerateHeader(char packetType, quint16 dataLen);
private slots:
    void slotOnInData();
};

#endif // UDPPROTOCOLMANAGER_H

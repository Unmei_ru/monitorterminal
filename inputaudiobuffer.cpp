#include "inputaudiobuffer.h"
#include <QThread>
#include <QDebug>

#define BYTES 320
#define MAX_SYNC 3


InputAudioBuffer::InputAudioBuffer(QObject *parent) : QObject(parent)
{

}

void InputAudioBuffer::slotAppendTo(QByteArray buf)
{
    m_mutex.lock();
    m_buffer.append(buf);
    m_mutex.unlock();

}

void InputAudioBuffer::slotPorcess()
{
     while(true) {

    m_mutex.lock();
    if (m_buffer.length() >= BYTES){
        emit signalMicData(m_buffer.mid(0,BYTES ));
        m_buffer.remove(0, BYTES );
        qDebug() << etimer.restart() << m_buffer.length();

        if (m_buffer.length() > BYTES * MAX_SYNC){

            m_buffer.clear();
            m_mutex.unlock();
            continue;
        }
    }
    m_mutex.unlock();

    QThread::msleep(10);

    }
}

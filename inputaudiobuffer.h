#ifndef INPUTAUDIOBUFFER_H
#define INPUTAUDIOBUFFER_H

#include <QObject>
#include <QMutex>
#include <QElapsedTimer>

class InputAudioBuffer : public QObject
{
    Q_OBJECT
public:
    explicit InputAudioBuffer(QObject *parent = nullptr);

private:
    QByteArray m_buffer;
    QMutex m_mutex;

     QElapsedTimer etimer;
signals:

    void signalMicData(QByteArray micData);
public slots:
    void slotAppendTo(QByteArray buf);
    void slotPorcess();
};

#endif // INPUTAUDIOBUFFER_H


MainWindowMaterial {
    id: window

    width: 1024
    height: 768

    visibility: main_IS_FULLSCREEN ? "FullScreen" : "Normal"
    flags: main_IS_FULLSCREEN ?  Qt.FramelessWindowHint : ""

}

import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3

import ConnectionManager 1.0

Item {
    id: materialButton

    property var _pin : 0
    property var _text : ""
    property var _color : "#d6d7d7";

    //Дополнительный режима
    property var _ip : ""
    property var _toggled : false
    property var _mode : 0
    property var _call: false
    property var _talk: false
    property var _code : 0

    Component.onCompleted: {
      mainWindow.listButtons.push( this );
      updateMode();
    }

    function setEnabed( mode ){
            materialButton.enabled = mode;
    }

    function setMode( mode ){
            _mode = mode;
    }

    function setCall( call ){
            _call = call;
    }

    function setTalk( talk ){

        if (_talk && !talk ){ //Отправка отбоя

        }
            _talk = talk;
    }

    function setToggled( check ){
            _toggled = check;
    }

    function setColor( colorId ){

        switch (colorId){
        case 0: //цвет “серый”
            _color = "gray" //"#d6d7d7"
            break;
        case 1: //цвет “желтый”
            _color = "yellow"//"#FFEB3B"
            break;
        case 2: //цвет “синий”
            _color = "blue"//"#1976D2"
            break;
        case 3: //цвет “зеленый”
            _color = "green" //"#4CAF50"
            break;
        case 4: //цвет “красный”
            _color = "red"//"#F44336"
            break;
        default: //цвет “серый”
            _color = "gray"//"#d6d7d7"
        }

    }

    function setBlink( blink ){

        switch ( blink ) {
        case 0:
            blinkAnim.stop();
            button._buttonColor = _color;
            break;
        case 1:
            blinkAnim.start();
            break;
        default:
            blinkAnim.stop();
            _buttonColor = _color;
            break;
        }
    }

    function getPin(){
        return _pin;
    }

    function getTalk(){
        return _talk;
    }

    function getMode(){
        return _mode;
    }

    function getIP(){
        return _ip;
    }

    function getCall(){
        return _call;
    }

    function getTitle(){
        return _text;
    }

    function getCode(){
        return _code;
    }

    function updateMode(){

        console.log("Update = " + _mode + " " + _pin);

        switch (_mode) {
        case 0:
            break;
        case 4:
        case 1: //Кнопка тип A
            setColor(3);//Переводим в зеленый цвет
            ConnectionManager.addPingIp(_ip)
            break;
        case 2: //Кнопка тип B
            setColor(3);//Переводим в зеленый цвет
            break;
        default:
            break;
        }

    }

    function endCall(){

        var ip = _ip;
        var packetType = 2; //Это "управление"
        var data = new ArrayBuffer(1);
        var bufferCtl = new Uint8Array(data);
        //Сгенерируем отбой, если говорим
        bufferCtl[0] = 3;
        ConnectionManager.sendAddonMessage(1, packetType, ip,  data);
    }


    SequentialAnimation {
        id: blinkAnim
        running: false

        loops: Animation.Infinite

        PropertyAnimation {
            target: button;

            property: "_buttonColor";
            to: "white";
            duration: 600
            onStopped: animationTwo.start()
        }
        PropertyAnimation {
            target: button;

            property: "_buttonColor";
            to: _color;
            duration: 600
            onStopped: animationOne.start()
        }
    }

    Button{
        id: button

        property var _buttonColor : _color


        anchors.fill: parent

        Material.background: _buttonColor

        Text {
            id: buttonText

            width: parent.width * 0.70
            height: parent.height * 0.70

            text: _text
            minimumPointSize: 1
            font.pointSize: 12
            fontSizeMode: Text.Fit
            wrapMode: Text.WordWrap

            anchors.centerIn: parent
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter

        }

        onClicked: {



            var pin = _pin;
            var directive = 3;
            var data = null;
            var ip = _ip;
            var packetType = 0;

            switch (_mode) {
            case 0:
                ConnectionManager.sendMessage(pin, directive,  data);
                break;
            case 1: //A
            case 4:
            {
                packetType = 2; //Это "управление"
                data = new ArrayBuffer(1);
                var bufferCtl = new Uint8Array(data);

                if (_talk){ //Сгенерируем отбой, если говорим
                bufferCtl[0] = 3;
                }else if (_call){ //Сгенерируем поднятие трубки если звонок
                bufferCtl[0] = 2;
                }else{ //В остальных случаях делаем вызов
                bufferCtl[0] = _mode;
                }
                ConnectionManager.sendAddonMessage(1, packetType, ip,  data);
                break;
            }
            case 2://B

                packetType = 3; //Это "команда"
                data = new ArrayBuffer(2);
                var bufferCmd = new Uint8Array(data);

                bufferCmd[0] = _code;


                if (_toggled){ //Если уже нажата
                bufferCmd[1] = 0; //Сбрасываем
                }else{
                bufferCmd[1] = 1; //Устанавливаем
                }

                ConnectionManager.sendAddonMessage(2, packetType, ip,  data);
                break;
            default:
                break;
            }

             _toggled = !_toggled;
        }

    }
}

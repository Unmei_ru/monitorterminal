import QtQuick 2.9
import QtQuick.Extras 1.4
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3

RowLayout{
    id: materialStatusIndicator
    property var  _pin
    property var  _text
    property var  _color : "#d6d7d7"
    property var  _active : false

    Component.onCompleted: {
      mainWindow.listIndicators.push( this )
    }

    spacing: 10
    scale: 0.7

    function setEnabed( mode ){
            _active = mode;
    }

    function setColor( colorId ){

        switch (colorId){
        case 0: //цвет “серый”
            _color = "#d6d7d7"
            break;
        case 1: //цвет “желтый”
            _color = "#FFEB3B"
            break;
        case 2: //цвет “синий”
            _color = "#1976D2"
            break;
        case 3: //цвет “зеленый”
            _color = "#4CAF50"
            break;
        case 4: //цвет “красный”
            _color = "#F44336"
            break;
        default: //цвет “серый”
            _color = "#d6d7d7"
        }

    }

    function setBlink( blink ){
        switch ( blink ) {
          case 0:
            blinkAnim.stop();
            break;
          case 1:
            blinkAnim.start();
            break;
          default:
            blinkAnim.stop();
        }
    }

    function getPin(){
        return _pin;
    }

    SequentialAnimation {
        id: blinkAnim
        running: false

        loops: Animation.Infinite

        PropertyAnimation {
            target: indicator;

            property: "active";
            to: 1;
            duration: 600
            onStopped: animationTwo.start()
        }
        PropertyAnimation {
            target: indicator;

            property: "active";
            to: 0;
            duration: 600
            onStopped: animationOne.start()
        }
    }

    StatusIndicator {
        id: indicator
        color: _color
        active: _active

    }

    Label {
        text: _text
        font.pixelSize: 20

        horizontalAlignment: Label.AlignHCenter
        verticalAlignment: Label.AlignVCenter

    }
}

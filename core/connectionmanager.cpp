#include "connectionmanager.h"
#include <QDebug>
#include <QTimer>

#ifdef __linux__
    //linux code goes here
#include <unistd.h>
#include <linux/reboot.h>
#include <sys/reboot.h>
#elif _WIN32
    // windows code goes here
#else

#endif

#define HEADER_LEN 3
#define CRC_LEN 2

ConnectionManager::ConnectionManager(AbstractAdapter *adapter, QObject *parent) : QObject(parent),
    m_adapter(adapter)
{

   QThread *threadUDP = new QThread(this);
   m_addonManagerUDP = new UDPProtocolManager();
   m_addonManagerUDP->moveToThread(threadUDP);
   connect(threadUDP, &QThread::started, m_addonManagerUDP, &UDPProtocolManager::slotInit );
   threadUDP->start();


   connect(m_addonManagerUDP, &UDPProtocolManager::signalContolMsg,
           this, &ConnectionManager::slotOnControlMsg, Qt::DirectConnection);

   m_audioManager = new AddonAudioManager();
   QThread *audioThread = new QThread(this);
   m_audioManager->moveToThread(audioThread);
   audioThread->start();

   connect(m_addonManagerUDP, &UDPProtocolManager::signalPlayAudio,
           m_audioManager, &AddonAudioManager::slotPlayAudio);
   connect(m_audioManager, &AddonAudioManager::signalMicData,
           m_addonManagerUDP, &UDPProtocolManager::slotSendAudio);

   connect(m_adapter, &AbstractAdapter::signalDataRecived,
           this, &ConnectionManager::slotOnDataRecived);
   connect(this, &ConnectionManager::signalDataSend,
           m_adapter, &AbstractAdapter::slotDataSend);

   m_playlist = new QMediaPlaylist();
   m_playlist->addMedia(QUrl("qrc:/main/call.wav"));
   m_playlist->setPlaybackMode(QMediaPlaylist::Loop);

   m_music = new QMediaPlayer();
   m_music->setPlaylist(m_playlist);

   QTimer::singleShot(10000, this, SLOT(slotSendIP()) );

   QTimer *timer = new QTimer(this);

   connect(timer, &QTimer::timeout, this, &ConnectionManager::slotCheckPing);

   timer->start(1000);
}

void ConnectionManager::sendMessage(int pin, int directive, QByteArray msgData)
{
    qDebug() << "SendMessage pin=" << pin << "directive:" << directive << "data:" << msgData.toHex();
    //Формирование пакета
    QByteArray out;
    //PIN
    uint16_t pin16 = pin;
    uint8_t pin16_h = (pin16 >> 8) & 0x00FF;
    uint8_t pin16_l = pin16 & 0x00FF;
    out.append( pin16_h );
    out.append( pin16_l );
    //DIRECTIVE
    uint8_t directive8 = directive;
    out.append( directive8 );
    //DATA
    out.append( msgData );
    //CRC
    quint16 ccrc = 0xFFFF; //Инициализируем crc
    uint8_t len = out.length() + CRC_LEN; // Получаем длину ( длина данных + длина crc )
    ccrc = crc(len, ccrc); //Помещаем длину т.к. crc = длина + пин + директива + дата

    for (int index = 0; index < out.length() ; ++index) //Для всех данных в пакете
        ccrc  = crc( (uint8_t)out.at(index), ccrc);

    uint8_t ccrc_h = (ccrc >> 8) & 0x00FF;
    uint8_t ccrc_l = ccrc & 0x00FF;

    out.append( ccrc_h );
    out.append( ccrc_l );
    //L
    out.prepend( len );
    //MARK
    out.prepend(PACKET_MARK);

    emit signalDataSend( out );
}

void ConnectionManager::sendAddonMessage(int mode, int packetType, QString ip, QByteArray msgData)
{
    //Список эвентов:
    // 1 - пришел "вызов"
    // 2 - пришел "ответ"
    // 3 - пришел "отбой"
    QHostAddress targetIp(ip);

    switch (mode) {
    case MODE_A:{ //Кнопка А

        if (packetType == PACKET_CONTROL){

            if (msgData.size() > 0 ){
                char cmd = msgData.at(0);

                if ( (int) cmd == 4) msgData[0] = (char) 0x1;
                qDebug() << "PACKET_CONTROL" << (int)cmd << msgData.toHex().data();
                m_addonManagerUDP->WriteControl(msgData, targetIp);
                m_music->stop();
                m_audioManager->StopCall();
                m_addonManagerUDP->ClearBuffer();


                switch (cmd) {
                case event_reciveCall:
                    m_addonManagerUDP->AddHost(targetIp);
                    emit signalAddonEvent(MODE_A, ip, event_reciveReply, 0);
                    break;
                case event_reciveCall_sound:
                    m_addonManagerUDP->AddHost(targetIp);
                    m_audioManager->PlayCall();
                    emit signalAddonEvent(MODE_A, ip, event_reciveReply, 0);
                    break;
                case event_reciveReply:
                    //m_addonManagerUDP->ClearBuffer();
                    m_addonManagerUDP->AddHost(targetIp);
                    emit signalAddonEvent(MODE_A, ip, event_reciveReply, 0);
                    break;
                case event_reciveEnd:
                    m_addonManagerUDP->RemoveHost(targetIp);
                    emit signalAddonEvent(MODE_A, ip, event_reciveEnd, 0);
                    break;
                default:
                    break;
                }
            }

        }
        break;
    }
    case MODE_B:{ //Кнопка Б

        if (packetType == PACKET_COMMAND){

            if (msgData.size() > 1 ){ //Должен содержать 2 байта
                char code = msgData.at(0);
                char param = msgData.at(1);
                qDebug() << "PACKET_COMMAND" << (int)code << (int)param << msgData.toHex().data() << targetIp;
                m_addonManagerUDP->WriteCommand(msgData, targetIp);

                emit signalAddonEvent(MODE_B, ip, param, code);
            }
        }

        break;
    }
    default:
        break;
    }
}

void ConnectionManager::openConnection()
{
    if (!m_adapter->Open()) {
        qDebug() << "Connecting error ";
        emit signalError("Ошибка подключения!");
    }
}

void ConnectionManager::reboot()
{

#ifdef __linux__
    //linux code goes here
	::sync();
	::reboot(RB_AUTOBOOT);
#elif _WIN32
    // windows code goes here
#else

#endif
}

void ConnectionManager::addPingIp(QString ip)
{
    QHostAddress host(ip);
    m_addonManagerUDP->AddButton(host);
}

void ConnectionManager::setAudioVolume(int num)
{
  m_audioManager->slotSetSpeakerVolume(num);
}

void ConnectionManager::setAudioMicVolume(int num)
{
  m_audioManager->slotSetMicVolume(num);
}

void ConnectionManager::slotOnDataRecived(QByteArray msg)
{
    m_buffer.append( msg );

    qDebug() << "DataRecived:" << msg.toHex();
    //Парсер
    //Пакет имеет следующий байтовый формат:
    //{0x16,0x16,L,PIN_h,PIN_l,DIRECTIVE,data[0]….. data[n],hcrc,lcrc}
    //Где :
    //0x16,0x16 – синхрометка;
    //L – кол-во байт в пакете минус синхрометка и байт длинны (L=весь пакет - 3);
    //PIN_h -PIN_l - старшая и младшая часть PINа устройства (кнопка, линия , 0 системное);
    //DIRECTIVE – код сообщения;
    //data[0]….. data[n] – данные сообщения;
    //hcrc_lcrc старшая и младшая часть CRC (расчете берутся данные PIN_h,PIN_l,DIRECTIVE,data[0]…..
    //data[n]);

   int index = 0;

   while ( true ) {

       index = m_buffer.indexOf(PACKET_MARK);

       if (index < 0 ) return; //Если не найдена синхромета
       m_buffer.remove(0, index); //Удаляем все лишнее до положения метки начала пакета
       if (m_buffer.length() < HEADER_LEN) return;

       quint8 len = m_buffer.at(2); // Считываем длину пакета ( длина - всеь пакет без синхрометки и байта длины )
       //qDebug() << "читаем длину" << len;

       if (m_buffer.length() - HEADER_LEN < len) return; // Проверка на то что данных без учетка заголовка достаточно для чтения (len) - длина пакета
       m_buffer.remove(0, HEADER_LEN);

       //qDebug() << "удаляем заголовок" << m_buffer.toHex();

       QByteArray packet =  m_buffer.left( len ); //Получаем итоговый пакет ( пин, директива, дата и crc )
       qDebug() << "Packet :" << packet.toHex() << ",len = " << len;
       m_buffer.remove(0, len); //Удаляем из буфера этот пакет
       //qDebug() << "Текущий буфер" << m_buffer.toHex();

       //Проверка crc пакета
       quint16 rcrc = (((quint16)packet.at(len - 2)) << 8) + (quint8) packet.at(len - 1); //СRC - последние два байта пакета

       //CRC
       quint16 ccrc = 0xFFFF;
       ccrc = crc(len, ccrc);
       for (int index = 0; index < (len - 2); ++index) //Проверяем crc пакета без учета crc иначе получим 0
           ccrc  = crc(packet.at(index), ccrc);

       qDebug() << "CRC:" << QString::number(ccrc, 16);


       emit signalCrcChecked(rcrc, ccrc);
       if (rcrc != ccrc) {
           qDebug() << "CRC error recived = " << rcrc << "calulated = " << ccrc;
           return;
       }

       quint16 pin = (((quint16)packet.at(0)) << 8) +(quint8) packet.at(1);
       int directive = packet.at(2);

       QByteArray msgData = packet.mid(3, len - 5);
       emit signalMessageRecived(pin, directive, msgData);
   }

}

void ConnectionManager::slotOnControlMsg(quint8 msgCode, QHostAddress host)
{

    switch (msgCode) {
    case MSG_CALL:
        if (m_addonManagerUDP->isHostsEmpty()) m_music->play();
        emit signalAddonEvent(MODE_A, host.toString(), event_reciveCall, 0);
        break;
    case MSG_REPlY:
        m_audioManager->StopCall();
        m_addonManagerUDP->ClearBuffer();
        m_music->stop();
        m_addonManagerUDP->AddHost(host);
        emit signalAddonEvent(MODE_A, host.toString(), event_reciveReply, 0);
        break;
    case MSG_END:
        m_audioManager->StopCall();
        m_addonManagerUDP->ClearBuffer();
        m_music->stop();
        m_addonManagerUDP->RemoveHost(host);
        emit signalAddonEvent(MODE_A, host.toString(), event_reciveEnd, 0);
        break;
    default:
        break;
    }
}

void ConnectionManager::slotSendIP()
{
    emit signalIPInfo(m_addonManagerUDP->localIp().toString());
}

void ConnectionManager::slotCheckPing()
{

    emit signalPingEnable();

    QList<quint32> failList = m_addonManagerUDP->CheckPing();

    foreach (quint32 ip4, failList) {

        QHostAddress host(ip4);

        emit signalPingFail(host.toString());
    }

    m_addonManagerUDP->WritePing();
}

uint16_t ConnectionManager::crc(uint8_t cData,uint16_t iCrc)
{
    uint8_t cPro;
    for (cPro=8;cPro;cPro--)
    {
        if ((cData & 0x80)!=((iCrc>>8)&0x80)) iCrc=(iCrc<<1)^0x8005;
        else iCrc=iCrc<<1;
        cData=cData<<1;
    }
    return(iCrc);
}

#ifndef ABSTRACTADAPTER_H
#define ABSTRACTADAPTER_H

#include <QObject>

class AbstractAdapter : public QObject
{
    Q_OBJECT
public:
    explicit AbstractAdapter(QObject *parent = nullptr);
    virtual bool Open() = 0;
signals:
    void signalDataRecived( QByteArray msg );
public slots:
    virtual void slotDataSend( QByteArray msg) = 0;
};

#endif // ABSTRACTADAPTER_H

#ifndef SERIALPORTADAPTER_H
#define SERIALPORTADAPTER_H

#include "abstractadapter.h"

#include <QSerialPort>
#include <QSerialPortInfo>
#include <QSettings>

class SerialPortAdapter : public AbstractAdapter
{
    Q_OBJECT
public:
    explicit SerialPortAdapter(QObject *parent = nullptr);
    bool Open();
private:
    QSerialPort *m_port;
signals:

public slots:
    void slotDataSend( QByteArray msg);

private slots:
    void slotOnReadyRead();
};

#endif // SERIALPORTADAPTER_H

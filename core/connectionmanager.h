#ifndef CONNECTIONMANGAGER_H
#define CONNECTIONMANGAGER_H

#include <QObject>
#include <QMediaPlaylist>
#include <QMediaPlayer>

#include "abstractadapter.h"
#include "udpprotocolmanager.h"
#include "addonaudiomanager.h"

const QByteArray PACKET_MARK = "\x16\x16";

#define MODE_A 1
#define MODE_B 2

class ConnectionManager : public QObject
{
    Q_OBJECT
public:
    enum AddonEvent{
        event_reciveCall = 1,
        event_reciveReply = 2,
        event_reciveEnd = 3,
        event_reciveCall_sound = 4
    };

    explicit ConnectionManager(AbstractAdapter *adapter, QObject *parent = nullptr);
    Q_INVOKABLE void sendMessage(int pin, int directive, QByteArray msgData);
    //Дополнительный протокол
    Q_INVOKABLE void sendAddonMessage(int mode, int packetType, QString ip, QByteArray msgData);
    Q_INVOKABLE void openConnection();
	Q_INVOKABLE void reboot();

    Q_INVOKABLE void addPingIp(QString ip);

    Q_INVOKABLE void setAudioVolume(int num);
    Q_INVOKABLE void setAudioMicVolume(int num);
private:
    AbstractAdapter *m_adapter; //Адаптер для работы основного протокола обмена для работы кнопок
    UDPProtocolManager *m_addonManagerUDP; //Для управление дополнительным UDP каналом управления/аудио
    AddonAudioManager *m_audioManager;
    QByteArray m_buffer;

    QMediaPlaylist *m_playlist;
    QMediaPlayer *m_music;

    uint16_t crc(uint8_t cData,uint16_t iCrc);
private slots:
    void slotOnDataRecived( QByteArray msg );
    void slotOnControlMsg(quint8 msgCode, QHostAddress host);
    void slotSendIP();

    void slotCheckPing();
signals:
    void signalDataSend( QByteArray msg );
    void signalMessageRecived( int pin, int directive, QByteArray msgData);
    void signalCrcChecked(int recivedCrc, int calculatedCrc);
    void signalError(QString msg);
    //Дополнительный протокол
    void signalAddonEvent(int mode, QString ip, int eventId, int code);
    void signalIPInfo(QString ip);
    void signalPingEnable();
    void signalPingFail(QString ip);

public slots:
};

#endif // CONNECTIONMANGAGER_H

#include "serialportadapter.h"
#include <QDebug>

SerialPortAdapter::SerialPortAdapter(QObject *parent) : AbstractAdapter(parent)
{
}

bool SerialPortAdapter::Open()
{
    m_port = new QSerialPort(this);
    connect(m_port, &QSerialPort::readyRead, this, &SerialPortAdapter::slotOnReadyRead);

    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("RS232");

    QString portName =  settings.value("PortName", "COM1").toString();
    qint32 baudRate =  settings.value("BaudRate", "115200").toInt();
    settings.endGroup();

    m_port->setPortName( portName );
    m_port->setBaudRate( baudRate );

    return m_port->open(QIODevice::ReadWrite);
}

void SerialPortAdapter::slotDataSend(QByteArray msg)
{
    m_port->write( msg );
}

void SerialPortAdapter::slotOnReadyRead()
{
    QByteArray buf = m_port->readAll();
    emit signalDataRecived( buf );
}

#ifndef AUDIOPROCESSOR_H
#define AUDIOPROCESSOR_H

#include <QObject>
#include <QTimer>

#include "webrtc/modules/audio_processing/include/audio_processing.h"
#include "webrtc/modules/include/module_common_types.h"

#include <QElapsedTimer>

#include <QMutex>

class AudioProcessor : public QObject
{
    Q_OBJECT
public:
    explicit AudioProcessor(QObject *parent = nullptr);

signals:
    void signalResultData(QByteArray result);
    void signalDataIn();
public slots:
    void slotOnMicData(QByteArray micData);
    void slotOnSpeakerData(QByteArray speakerData);
    void slotOnProcess();
private:
    QByteArray m_micBuffer;
    QByteArray m_speakerBuffer;

    webrtc::AudioProcessing* m_apm;

    QMutex m_mutex;

    QElapsedTimer testTimer;

    bool ReadFrame(QByteArray *dataIn, webrtc::AudioFrame *frame);
    bool WriteFrame(QByteArray *dataOut, webrtc::AudioFrame *frame);

    bool WebRTCInit();
private slots:

};

#endif // AUDIOPROCESSOR_H

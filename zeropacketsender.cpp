#include "zeropacketsender.h"
#include <QThread>

#define MS_DELAY 10
#define MAX_WAIT 100

ZeroPacketSender::ZeroPacketSender(QObject *parent) : QObject(parent)
{
    m_elapsedTimer = new QElapsedTimer();
    m_waitSync = true;
}

void ZeroPacketSender::Sync()
{
    m_waitSync = false;
    m_elapsedTimer->restart();
}

void ZeroPacketSender::slotSendZeroPackets()
{
    while (true) {

        QThread::msleep(MS_DELAY);

        if (m_elapsedTimer->elapsed() > MAX_WAIT) m_waitSync = true;

        if (m_waitSync){

            QByteArray buffer(320, char(0));

            emit signalZeroAudio(buffer);

        }
    }
}

#include "audioinputreader.h"
#include <QThread>
#include <QDebug>

AudioInputReader::AudioInputReader(QIODevice *input, QObject *parent) : QObject(parent)
{
    m_audioInput = input;
}

void AudioInputReader::slotReadBuffer()
{

    m_audioInput->readAll();

    while (true) {

            QThread::msleep(15);
            emit signalDataRead(m_audioInput->read(320));
    }

}

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QSettings>
#include <QQuickStyle>
#include <QScreen>
#include <QSettings>
#include <QFont>
#include <QFontDatabase>
#include <QLoggingCategory>
#include <QFile>
#include <QDateTime>
#include <QCommandLineParser>
#include <QCommandLineOption>
// Умный указатель на файл логирования
QScopedPointer<QFile>   m_logFile;

// Объявляение обработчика
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);


#include "core/connectionmanager.h"
#include "core/serialportadapter.h"

static QObject *connectionManagerSingletonTypeProvider(QQmlEngine *engine, QJSEngine *scriptEngine)
{
    Q_UNUSED(engine)
    Q_UNUSED(scriptEngine)

    ConnectionManager *connectionManager = new ConnectionManager(new SerialPortAdapter());

    return connectionManager;
}



int main(int argc, char *argv[])
{


    QGuiApplication::setApplicationName("Terminal_monitor");
    QGuiApplication::setOrganizationName("Home");


    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qRegisterMetaType<QHostAddress>("QHostAddress");

    QCommandLineParser parser;
    parser.setApplicationDescription("Description: Terminal monitor.");

    QCommandLineOption setSettingsDirectory(QStringList() << "d" << "dir", "Path for settings directory", "settings", "0");
    parser.addOption(setSettingsDirectory);
    parser.process(app);

    const QStringList args = parser.optionNames();
    QString optDir = "";

    qDebug() << args;

    if (args.size() >= 1) {
        optDir = QString("%1/").arg(parser.value(setSettingsDirectory));
    }

    QQuickStyle::setStyle("Material");
    qmlRegisterSingletonType<ConnectionManager>("ConnectionManager", 1, 0, "ConnectionManager", connectionManagerSingletonTypeProvider);

    QScreen *screen = qApp->primaryScreen();
    int dpi = screen->physicalDotsPerInch() * screen->devicePixelRatio();
    // now calculate the dp ratio
    qreal dp = dpi / 160.f;


    qDebug() << "optDir" << optDir;
    QSettings settings( optDir + "settings.ini", QSettings::IniFormat);
    settings.beginGroup("COLOR");

    QString c_DARK_PRIMARY_COLOR =  settings.value("DARK_PRIMARY_COLOR", "#1976D2").toString();
    QString c_PRIMARY_COLOR =  settings.value("PRIMARY_COLOR", "#2196F3").toString();
    QString c_LIGHT_PRIMARY_COLOR =  settings.value("LIGHT_PRIMARY_COLOR", "#BBDEFB").toString();
    settings.endGroup();

    settings.beginGroup("TEXT");
    QString titleText =  settings.value("Title", "Пульт оператора").toString();
    settings.endGroup();

    settings.beginGroup("MAIN");
    bool isFullScreen =  settings.value("FullScreen", true).toBool();
    bool isLogs = settings.value("Logging", false).toBool();
    settings.endGroup();

    if (isLogs){
    // Устанавливаем файл логирования
     m_logFile.reset(new QFile( optDir + "logFile_" + QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ") + ".txt"));
    // Открываем файл логирования
     m_logFile.data()->open(QFile::Append | QFile::Text);
     // Устанавливаем обработчик
     qInstallMessageHandler(messageHandler);
    }

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("dp", dp);

    engine.rootContext()->setContextProperty("c_DARK_PRIMARY_COLOR", c_DARK_PRIMARY_COLOR);
    engine.rootContext()->setContextProperty("c_PRIMARY_COLOR", c_PRIMARY_COLOR);
    engine.rootContext()->setContextProperty("c_LIGHT_PRIMARY_COLOR", c_LIGHT_PRIMARY_COLOR);

    engine.rootContext()->setContextProperty("text_TITLE", titleText);

    engine.rootContext()->setContextProperty("main_IS_FULLSCREEN", isFullScreen);

    engine.rootContext()->setContextProperty("main_DIR", optDir);

    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}

// Реализация обработчика
void messageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    // Открываем поток записи в файл
    QTextStream out(m_logFile.data());
    // Записываем дату записи

    if (type == QtWarningMsg) return;

    out << QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss.zzz ");
    // По типу определяем, к какому уровню относится сообщение

    switch (type)
    {
    case QtInfoMsg:     out << "INF "; break;
    case QtDebugMsg:    out << "DBG "; break;
    case QtCriticalMsg: out << "CRT "; break;
    case QtFatalMsg:    out << "FTL "; break;
    }
    // Записываем в вывод категорию сообщения и само сообщение
    out << context.category << ": "
        << msg << endl;
    out.flush();    // Очищаем буферизированные данные
}

import QtQml.Models 2.2
import QtQuick 2.6
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3

import ConnectionManager 1.0

Page {
    id: page

    JSONListModel {
        id: jsonModel1
        source: "file:" +main_DIR + "jsonData.json"

        query: "$.terminal.subscriber[*]"

    }

    Component.onCompleted: ConnectionManager.openConnection()

    Component {
        id: subsDelegate

        ButtonMaterial {
            id: button
            _pin : pin
            _text: title
            _color: color

            _mode:  mode
            _ip: ip

            _code: code

            implicitWidth:  (parent.width / 4) - 4
            implicitHeight: (parent.height / 6) - 6

            Layout.column: pos_x
            Layout.row: pos_y
        }
    }

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        clip: true


        Repeater {

            model: jsonModel1.model

            Pane {

                width: swipeView.width
                height: swipeView.height

                JSONListModel {
                  id: buttonModel
                  source: "file:" + main_DIR + "jsonData.json"
                  query: "$.terminal.subscriber["+ index +"].button[*]"

                }

                GridLayout{
                    id: tabGrid
                    anchors.fill: parent
                    columns: 4
                    rows: 6


                    Repeater{
                        model: 24
                        Item{
                            Layout.fillWidth: true
                            Layout.fillHeight: true
                        }
                    }

                    Repeater{
                        model: buttonModel.model
                        delegate: subsDelegate

                    }
                }
              }
        }
    }

    header: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        Repeater {
            model: jsonModel1.model

        TabButton {
            text: model.title
        }
        }

    }
}


#include "udpprotocolmanager.h"

#include <QThread>
#include <QNetworkDatagram>
#include <QNetworkInterface>
#include <QTimer>

#define AUDIO_BYTES 320

UDPProtocolManager::UDPProtocolManager(QObject *parent) : QObject(parent)
{

    connect(this, &UDPProtocolManager::signalWriteDatagram, this, &UDPProtocolManager::slotWriteDatagram);

    m_counter = 0;

    qDebug() << "local IP" << m_localIp;


    //QThread *zeroThread = new QThread(this);
    //m_zeroSender = new ZeroPacketSender();
    //m_zeroSender->moveToThread(zeroThread);
    //connect(zeroThread, &QThread::started, m_zeroSender, &ZeroPacketSender::slotSendZeroPackets);
    //connect(m_zeroSender, &ZeroPacketSender::signalZeroAudio, this, &UDPProtocolManager::signalPlayAudio);
    //zeroThread->start();
}

void UDPProtocolManager::AddButton(QHostAddress host)
{
    m_buttonsList.append(host.toIPv4Address());
    m_okList.append(0);
}


void UDPProtocolManager::StartUDPServer()
{

    const QHostAddress &localhost = QHostAddress(QHostAddress::LocalHost);
    for (const QHostAddress &address: QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != localhost)
            m_localIp = address;
    }

    qDebug() << m_localIp;

    m_socket->bind(m_localIp, SERVER_PORT);
}

void UDPProtocolManager::AddHost(QHostAddress host)
{
    m_mutex.lock();
    qDebug() << "AddHost" << host.toString();

    quint32 ip4Host = host.toIPv4Address();

    if (!m_hostsList.contains(ip4Host))
        m_hostsList.append(ip4Host);
    m_mutex.unlock();
}

void UDPProtocolManager::RemoveHost(QHostAddress host)
{
    m_mutex.lock();
    qDebug() << "RemoveHost" << host.toString();

    quint32 ip4Host = host.toIPv4Address();
    m_hostsList.removeAll(ip4Host);

    m_mutex.unlock();
}

bool UDPProtocolManager::isHostsEmpty()
{
    return m_hostsList.isEmpty();
}

void UDPProtocolManager::WriteControl(QByteArray buf, QHostAddress host)
{
    QByteArray msg;
    msg.append(GenerateHeader(PACKET_CONTROL, buf.size()));
    msg.append(buf);
    emit signalWriteDatagram(msg, host, CLIENT_PORT);
}

void UDPProtocolManager::WriteCommand(QByteArray buf, QHostAddress host)
{
    QByteArray msg;
    msg.append(GenerateHeader(PACKET_COMMAND, buf.size()));
    msg.append(buf);
    emit signalWriteDatagram(msg, host, CLIENT_PORT);
}

void UDPProtocolManager::WritePing()
{

    for (int index = 0; index < m_buttonsList.length(); ++index) {

        quint32 ip4_button = m_buttonsList.at(index);
        QByteArray buf;
        buf.append(  (char) index );

        QHostAddress host(ip4_button);

        QByteArray msg;
        msg.append(GenerateHeader(PACKET_PING, buf.size()));
        msg.append(buf);
        emit signalWriteDatagram(msg, host, CLIENT_PORT);

        //qDebug() << "Send ping" << index << msg.toHex().data() << host;

    }

}

QList<quint32> UDPProtocolManager::CheckPing()
{

    m_mutex.lock();

    QList<quint32> failList;

    for (int index = 0; index < m_okList.length(); ++index) {

        quint32 ok = m_okList.at(index);

        if (ok > 0 ){
            m_okList[index] = 0;
        }else{
            if (m_buttonsList.size() > index){
                failList.append( m_buttonsList.at(index) );
            }
        }

    }

    m_mutex.unlock();

    return failList;
}


void UDPProtocolManager::slotInit()
{
    m_socket = new QUdpSocket();
    connect(m_socket, &QUdpSocket::readyRead, this, &UDPProtocolManager::slotOnInData);

    StartUDPServer();
}

void UDPProtocolManager::slotSendAudio(QByteArray audioData)
{
    m_mutex.lock();
    if (!m_hostsList.isEmpty()){

        if (audioData.size() >= AUDIO_BYTES){

            foreach (quint32 ip4Host, m_hostsList) {
                QByteArray msg = GenerateHeader(PACKET_AUDIO, AUDIO_BYTES);
                msg.append(audioData.data(), AUDIO_BYTES);
                emit signalWriteDatagram(msg ,QHostAddress(ip4Host), CLIENT_PORT);
            }
            //audioData.remove(0, AUDIO_BYTES);

            //qDebug() << "packet time" << m_packetTimer.restart();

            }
    }else {
        m_audioBuffer.clear();
    }
    m_mutex.unlock();
}

void UDPProtocolManager::slotWriteDatagram(const QByteArray &datagram, const QHostAddress &host, quint16 port)
{
    m_socket->writeDatagram(datagram, host, port);
}

QHostAddress UDPProtocolManager::localIp() const
{
    return m_localIp;
}

void UDPProtocolManager::ClearBuffer()
{
    m_mutex.lock();
    m_audioBuffer.clear();
    m_mutex.unlock();
}

QByteArray UDPProtocolManager::GenerateHeader(char packetType, quint16 dataLen)
{
    QByteArray header;

    uint8_t len_h = (dataLen >> 8) & 0x00FF;
    uint8_t len_l = dataLen & 0x00FF;

    m_counter++;

    uint8_t counter_h = (m_counter >> 8) & 0x00FF;
    uint8_t counter_l = m_counter & 0x00FF;

    //длина данных
    header.append( len_h );
    header.append( len_l );
    //Индекс пакета отправленный в рамках связи с конкретным УПГ (2 байт младшая часть);
    header.append(counter_h);
    header.append(counter_l);
    //Индекс пакета принятый в рамках связи с конкретным УПГ (4 байт младшая часть);
    header.append(QByteArray::fromHex("0000"));
    //тип пакета 1-звук,2-управление,3-команда
    header.append(packetType);

//    qDebug() << "GenerateHeader" << header.toHex().data();

    return header;
}

void UDPProtocolManager::slotOnInData()
{

    //qDebug() << m_packetTimer.restart();

    while (m_socket->hasPendingDatagrams())
    {
        QByteArray packet;
        packet.resize(m_socket->pendingDatagramSize());
        QHostAddress host;

        m_socket->readDatagram(packet.data(), packet.size(), &host);
        //qDebug() << "slotOnInData" << host.toString();

        quint32 ip4Host = host.toIPv4Address();

        if (packet.size() > PACKET_HEADER_SIZE){

            quint16 lenData         = (((quint16)packet.at(0)) << 8) + (quint8)packet.at(1);
            quint16 indexTransmited = (((quint16)packet.at(2)) << 8) + (quint8)packet.at(3);
            quint16 indexReceived   = (((quint16)packet.at(4)) << 8) + (quint8)packet.at(5);
            quint8  packetType      = (quint8)packet.at(6);

            switch (packetType) {
            case PACKET_AUDIO:{
                if (m_hostsList.contains(ip4Host)){
                    packet.remove(0, 7);
                    //m_zeroSender->Sync();
                    //qDebug() << "packet time" << m_packetTimer.restart();
                    emit signalPlayAudio( packet );
                }
                break;
            }
            case PACKET_CONTROL:{

                quint8  msgCode = (quint8)packet.at(7);

                qDebug() << "R PACKET_CONTROL" << msgCode;

                emit signalContolMsg(msgCode, host);
                break;
            }
            case PACKET_COMMAND:{
                if (packet.length() > 7){
                    quint8  commandCode    = (quint8)packet.at(7);
                    quint8  commandArg     = (quint8)packet.at(8);
                    //Реакция не описана?
                }
                break;
            }
            case PACKET_PING:{
                    quint8  pingCode    = (quint8)packet.at(7);

                    m_mutex.lock();
                    if (m_okList.length() > pingCode) {

                        m_okList[pingCode] = 1;
                    }

                    m_mutex.unlock();

                    //qDebug() << "In ping " << pingCode;

                    break;
                }
                default:
                    break;


            }
        }
    }
}

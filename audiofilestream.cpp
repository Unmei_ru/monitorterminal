#include "audiofilestream.h"
#include <QDebug>
#include <QThread>
#define AUDIO_BYTES 320

AudioFileStream::AudioFileStream() :QObject(0),
    m_state(State::Stopped)
{
    isInited = false;
    isDecodingFinished = false;
    m_sendCallData = false;
}

// format - it is audio format to which we whant decode audio data
bool AudioFileStream::init(const QAudioFormat& format)
{
    m_format = format;
    m_decoder.setAudioFormat(m_format);

    connect(&m_decoder, SIGNAL(bufferReady()), this, SLOT(bufferReady()));
    connect(&m_decoder, SIGNAL(finished()), this, SLOT(finished()));
    connect(this, &AudioFileStream::signalOnFinish, this, &AudioFileStream::slotGenerateCall);


    isInited = true;

    return true;
}

void AudioFileStream::SetCall(bool flag)
{
    m_sendCallData = flag;
}


// Start play audio file
void AudioFileStream::play(const QString &filePath)
{
    clear();

    m_file.setFileName(filePath);

    if (!m_file.open(QIODevice::ReadOnly))
    {
        qDebug() << "error file audio";
        return;
    }

    m_decoder.setSourceDevice(&m_file);
    m_decoder.start();

    m_state = State::Playing;
    emit stateChanged(m_state);
}

// Stop play audio file
void AudioFileStream::stop()
{
    clear();
    m_state = State::Stopped;
    emit stateChanged(m_state);
}


void AudioFileStream::clear()
{
    m_decoder.stop();
    isDecodingFinished = false;
}

// Run when decode decoded some audio data
void AudioFileStream::bufferReady() // SLOT
{
    const QAudioBuffer &buffer = m_decoder.read();

    const int length = buffer.byteCount();
    const char *data = buffer.constData<char>();

    QByteArray outData;
    outData.append(data, length);

    m_sendBuffer.append( outData );
}

// Run when decode finished decoding
void AudioFileStream::finished() // SLOT
{
    isDecodingFinished = true;

    stop();

    qDebug() << "Call decode ok" << m_sendBuffer.size();

    emit signalOnFinish();
}

void AudioFileStream::slotGenerateCall()
{

    while (true) {

        if (m_sendCallData){

            QByteArray audioBuffer = m_sendBuffer;

            while (audioBuffer.size() >= AUDIO_BYTES){

                QByteArray msg;
                msg.append(audioBuffer.data(), AUDIO_BYTES);
                audioBuffer.remove(0, AUDIO_BYTES);

                if (m_sendCallData){
                    emit newData(msg);
                    QThread::msleep(10);
                }
            }
        }

        //if (m_sendCallData) emit newData(audioBuffer);

        QThread::msleep(150);
    }
}




/////////////////////////////////////////////////////////////////////

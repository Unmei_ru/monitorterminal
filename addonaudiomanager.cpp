#include "addonaudiomanager.h"

#include <QDebug>
#include <QProcess>
#include <QSettings>
#include <QThread>

#include "inputaudiobuffer.h"



AddonAudioManager::AddonAudioManager(QObject *parent) : QObject(parent)
{
    QSettings settings("settings.ini", QSettings::IniFormat);
    settings.beginGroup("AUDIO");
    qint32 sampleRate =  settings.value("SampleRate", "16000").toInt();
    qint32 channelCount =  settings.value("ChannelCount", "1").toInt();
    qint32 sampleSize =  settings.value("SampleSize", "16").toInt();
    QString codec =  settings.value("Codec", "audio/pcm").toString();
    settings.endGroup();

    m_format.setSampleRate(sampleRate);
    m_format.setChannelCount(channelCount);
    m_format.setSampleSize(sampleSize);
    m_format.setCodec(codec);
    m_format.setByteOrder(QAudioFormat::LittleEndian);
    m_format.setSampleType(QAudioFormat::SignedInt);


    QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
    if (!info.isFormatSupported(m_format))
        m_format = info.nearestFormat(m_format);

    m_output = new QAudioOutput(m_format);
    m_deviceOutput = m_output->start();

    qDebug() << "deviceOutput" << m_format << m_deviceOutput;

    QThread *callThread = new QThread();
    m_fileStram = new AudioFileStream();
    m_fileStram->moveToThread(callThread);
    callThread->start();

    m_fileStram->init(m_format);
    connect(m_fileStram, &AudioFileStream::newData, this, &AddonAudioManager::slotOnNewData);

    //====================================================================
    //QAudioDeviceInfo infoIn(QAudioDeviceInfo::defaultInputDevice());
    //if (!infoIn.isFormatSupported(m_format))
    //   m_format = infoIn.nearestFormat(m_format);

    //m_input = new QAudioInput(m_format);
    //m_input->setBufferSize(320);
    //m_deviceInput = m_input->start();

    //qDebug() << "deviceInput" << m_format << m_deviceInput;


    //connect(m_deviceInput, &QIODevice::readyRead, this, &AddonAudioManager::slotOnMicDataReady);
    //=====================================================================


    m_alsaReader = new AudioReader();
    QThread *alsaThread = new QThread(this);
    m_alsaReader->moveToThread(alsaThread);
    connect(alsaThread, &QThread::started,
            m_alsaReader, &AudioReader::slotReadAudio);


    m_isPlay = false;
    m_fileStram->play( ":/main/call.wav" );

    QThread *processorThread = new QThread(this);
    m_audioProcessor = new AudioProcessor();
    m_audioProcessor->moveToThread(processorThread);
    connect(processorThread, &QThread::started,
            m_audioProcessor, &AudioProcessor::slotOnProcess);


    QThread *ioT = new QThread(this);

    InputAudioBuffer *aBuffer = new InputAudioBuffer();
    aBuffer->moveToThread(ioT);



    connect(ioT, &QThread::started,
            aBuffer, &InputAudioBuffer::slotPorcess);

    // From alsa to buffer
    connect(m_alsaReader, &AudioReader::signalMicData,
            aBuffer, &InputAudioBuffer::slotAppendTo, Qt::DirectConnection);

    // From buffer to processor (10 ms frame)
    connect(aBuffer, &InputAudioBuffer::signalMicData,
            m_audioProcessor, &AudioProcessor::slotOnMicData, Qt::DirectConnection);

    //From processor to audiomanager
    connect(m_audioProcessor, &AudioProcessor::signalResultData,
            this, &AddonAudioManager::slotOnProcessedMic);

    //From speaker to audio processor
    connect(this, &AddonAudioManager::signalInnerSpeakerData,
            m_audioProcessor, &AudioProcessor::slotOnSpeakerData, Qt::DirectConnection);



    ioT->start();
    processorThread->start();
    alsaThread->start();


}

void AddonAudioManager::PlayCall()
{
    qDebug() << "Play call";
    m_isPlay = true;
    m_fileStram->SetCall( true );

}

void AddonAudioManager::StopCall()
{
    qDebug() << "Stop call";
    m_isPlay = false;
    m_fileStram->SetCall( false );

}

void AddonAudioManager::slotPlayAudio(QByteArray audioData)
{
   m_deviceOutput->write(audioData.data(), audioData.size());
}

void AddonAudioManager::slotOnMicDataReady()
{
    //QByteArray micBuffer = m_deviceInput->readAll();
    //emit signalInnerMicData(micBuffer);

    //qDebug() << micBuffer.length();

}

void AddonAudioManager::slotOnProcessedMic(QByteArray micBuffer)
{
    if (!m_isPlay) {
        emit signalMicData(micBuffer); //send by UDP
    }
}

void AddonAudioManager::slotSetMicVolume(quint8 vol)
{
   QProcess::startDetached(QString("amixer set Capture %1%").arg(vol));

   qDebug() << QString("amixer set Capture %1%").arg(vol);
}

void AddonAudioManager::slotSetSpeakerVolume(quint8 vol)
{

   quint8 proc = vol * 0.7;

   QProcess::startDetached(QString("amixer set Master %1%").arg(proc));

   qDebug() << QString("amixer set Master %1%").arg(proc);
}

void AddonAudioManager::slotOnNewData(const QByteArray &data)
{
    if (m_isPlay)
    {
        emit signalMicData(data);
        //qDebug() << "call";
    }
}

#ifndef AUDIOREADER_H
#define AUDIOREADER_H

#include <QObject>
#include <stdio.h>
#include <stdlib.h>
#include <alsa/asoundlib.h>
#include <QElapsedTimer>

class AudioReader : public QObject
{
    Q_OBJECT
public:
    explicit AudioReader(QObject *parent = nullptr);

signals:
    void signalMicData(QByteArray audioData);

public slots:
    void slotReadAudio();
private:
    QByteArray buffer;
    snd_pcm_t *capture_handle;
    snd_pcm_hw_params_t *hw_params;
    snd_pcm_format_t m_format;

    int err;
    snd_pcm_uframes_t buffer_frames;
    unsigned int m_rate;

    QElapsedTimer etimer;

    int openCapture();
    int capturceFunc();

    int ca_buf_size;
    int input_latency_ms;
};

#endif // AUDIOREADER_H

#ifndef AUDIOINPUTREADER_H
#define AUDIOINPUTREADER_H

#include <QObject>
#include <QAudioInput>

class AudioInputReader : public QObject
{
    Q_OBJECT
public:
    explicit AudioInputReader(QIODevice  *input, QObject *parent = nullptr);
private:
    QIODevice *m_audioInput;

signals:
    void signalDataRead(QByteArray buffer);
public slots:

    void slotReadBuffer();
};

#endif // AUDIOINPUTREADER_H

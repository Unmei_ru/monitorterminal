#ifndef ADDONAUDIOMANAGER_H
#define ADDONAUDIOMANAGER_H

#include <QObject>
#include <QAudioFormat>
#include <QAudioOutput>
#include <QAudioInput>
#include <QAudioDeviceInfo>
#include "audiofilestream.h"
#include "audioprocessor.h"
#include "audioreader.h"


class AddonAudioManager : public QObject
{
    Q_OBJECT
public:
    explicit AddonAudioManager(QObject *parent = nullptr);

    void PlayCall();
    void StopCall();

signals:
    void signalMicData(QByteArray audioData);
    void signalCallData(QByteArray data);

    void signalInnerMicData(QByteArray audioData);
    void signalInnerSpeakerData(QByteArray audioData);
public slots:
    void slotPlayAudio(QByteArray audioData);
    void slotOnMicDataReady();
    void slotOnProcessedMic(QByteArray micBuffer);
    void slotSetMicVolume(quint8 vol);
    void slotSetSpeakerVolume(quint8 vol);

    void slotOnNewData(const QByteArray& data);


private:
    QAudioFormat m_format;
    QAudioOutput *m_output;
    QAudioInput  *m_input;
    QIODevice *m_deviceOutput;
    QIODevice *m_deviceInput;

    AudioProcessor *m_audioProcessor;
    AudioReader *m_alsaReader;

    quint32 m_micVol;
    quint32 m_speakerVol;

    AudioFileStream *m_fileStram;


    bool m_isPlay;

};

#endif // ADDONAUDIOMANAGER_H

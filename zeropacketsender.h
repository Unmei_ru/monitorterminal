#ifndef ZEROPACKETSENDER_H
#define ZEROPACKETSENDER_H

#include <QObject>
#include <QElapsedTimer>

class ZeroPacketSender : public QObject
{
    Q_OBJECT
public:
    explicit ZeroPacketSender(QObject *parent = nullptr);

    void Sync();

signals:
    void signalZeroAudio(QByteArray audioData);
public slots:

    void slotSendZeroPackets();
private:
    QElapsedTimer *m_elapsedTimer;

    bool m_waitSync;
};

#endif // ZEROPACKETSENDER_H

#include "audioprocessor.h"
#include <QTimer>
#include <QDebug>
#include <QThread>

#define MAX_BYTES 320

AudioProcessor::AudioProcessor(QObject *parent) : QObject(parent)
{
    //connect(this, &AudioProcessor::signalDataIn, this, &AudioProcessor::slotOnProcess);

    bool res = WebRTCInit();
}

bool AudioProcessor::ReadFrame(QByteArray* dataIn, webrtc::AudioFrame* frame) {
    // The files always contain stereo audio.
    size_t frame_size = frame->samples_per_channel_;

    if (dataIn->length() >= (frame_size * 2)){

        m_mutex.lock();
        memcpy(frame->data_, dataIn->data(), frame_size * 2);
        dataIn->remove(0, (frame_size * 2));
        m_mutex.unlock();

    }else{
        return false;
    }

    return true;
}

bool AudioProcessor::WriteFrame(QByteArray* dataOut, webrtc::AudioFrame* frame) {
    // The files always contain stereo audio.
    size_t frame_size = frame->samples_per_channel_;

    if (dataOut != nullptr){

        dataOut->append((char *)frame->data_, frame_size * 2);

    }else{
        return false;  // This is expected.
    }

    return true;
}

bool AudioProcessor::WebRTCInit()
{
    bool res = true;
    int delay_ms = 0;
    int nsLevel = 3;
    int agcLevel = 0;
    int aecLevel = 2;

    m_apm = nullptr;

    // Usage example, omitting error checking:
    m_apm = webrtc::AudioProcessing::Create();

    if (m_apm){

        webrtc::Config config;
        m_apm->level_estimator()->Enable(true);

        config.Set<webrtc::ExtendedFilter>(new webrtc::ExtendedFilter(true));
        config.Set<webrtc::DelayAgnostic>(new webrtc::DelayAgnostic(true));

        //Включаем и настраиваем эхоподавление
        m_apm->echo_cancellation()->Enable(true);
        //m_apm->echo_cancellation()->enable_metrics(true);
        m_apm->echo_cancellation()->enable_delay_logging(true);
        m_apm->set_stream_delay_ms(delay_ms);

        //m_apm->echo_cancellation()->enable_drift_compensation(true);
        m_apm->echo_cancellation()->set_suppression_level(webrtc::EchoCancellation::kHighSuppression);

        m_apm->SetExtraOptions(config);

        m_apm->high_pass_filter()->Enable(true);

        //Включение и настройка шумадава
        m_apm->noise_suppression()->Enable(true);
        switch (nsLevel) {
        case 0:
            m_apm->noise_suppression()->set_level(webrtc::NoiseSuppression::kLow);
            break;
        case 1:
            m_apm->noise_suppression()->set_level(webrtc::NoiseSuppression::kModerate);
            break;
        case 2:
            m_apm->noise_suppression()->set_level(webrtc::NoiseSuppression::kHigh);
            break;
        case 3:
            m_apm->noise_suppression()->set_level(webrtc::NoiseSuppression::kVeryHigh);
            break;
        default:
            m_apm->noise_suppression()->set_level(webrtc::NoiseSuppression::kVeryHigh);
        }
        m_apm->voice_detection()->Enable(true); //Нужно ли?

        //Включение регулировки усиления
        m_apm->gain_control()->Enable(true);
        m_apm->gain_control()->set_analog_level_limits(0, 255);
        switch (agcLevel) {
        case 0:
            m_apm->gain_control()->set_mode(webrtc::GainControl::kAdaptiveAnalog);
            break;
        case 1:
            m_apm->gain_control()->set_mode(webrtc::GainControl::kAdaptiveDigital);
            break;
        case 2:
            m_apm->gain_control()->set_mode(webrtc::GainControl::kFixedDigital);
            break;
        default:
            m_apm->gain_control()->set_mode(webrtc::GainControl::kAdaptiveAnalog);
        }

        //Настройка эходава

        switch (aecLevel) {
        case 0:
            m_apm->echo_cancellation()->set_suppression_level(webrtc::EchoCancellation::kLowSuppression);
            break;
        case 1:
            m_apm->echo_cancellation()->set_suppression_level(webrtc::EchoCancellation::kModerateSuppression);
            break;
        case 2:
            m_apm->echo_cancellation()->set_suppression_level(webrtc::EchoCancellation::kHighSuppression);
        }

    }else{
        res = false;
    }

    return res;
}

void AudioProcessor::slotOnMicData(QByteArray micData)
{
    m_mutex.lock();
    m_micBuffer.append(micData);
    m_mutex.unlock();

}

void AudioProcessor::slotOnSpeakerData(QByteArray speakerData)
{
    m_mutex.lock();
    m_speakerBuffer.append(speakerData);
    m_mutex.unlock();

}

void AudioProcessor::slotOnProcess()
{

    webrtc::AudioFrame far_frame;
    webrtc::AudioFrame near_frame;

    float frame_step = 10;  // ms
    int delay_ms = 60;
    int drift_samples = 15;

    far_frame.sample_rate_hz_ = 16000;
    far_frame.samples_per_channel_ = far_frame.sample_rate_hz_ * frame_step / 1000.0;
    far_frame.num_channels_ = 1;
    near_frame.sample_rate_hz_ = 16000;
    near_frame.samples_per_channel_ = near_frame.sample_rate_hz_ * frame_step / 1000.0;
    near_frame.num_channels_ = 1;

    QByteArray resultBuffer;

    while(true){

        if (m_micBuffer.length() < (MAX_BYTES))
        {
            QThread::usleep(1);
            continue;
        }

        ReadFrame(&m_micBuffer, &near_frame); //m_micBuffer

        ReadFrame(&m_speakerBuffer, &far_frame); //m_speakerBuffer

        m_apm->ProcessReverseStream(&far_frame);

        m_apm->set_stream_delay_ms(delay_ms);

        m_apm->echo_cancellation()->set_stream_drift_samples(drift_samples);

        int err = m_apm->ProcessStream(&near_frame);

        WriteFrame(&resultBuffer, &near_frame);

        if (!resultBuffer.isEmpty()) {

            emit signalResultData(resultBuffer);

            //qDebug() << "PRocess time" << testTimer.restart();

            resultBuffer.clear();
        }

    }


}


